[TOC]

**解决方案介绍**
===============
该解决方案基于开源软件MHA构建，快速帮助用户在华为云服务器上完成高可用的MySQL集群部署。MHA（Master High Availability）是一款成熟且开源的MySQL高可用程序，主要提供心跳检测、主从复制、故障转移，并发送告警邮件。适用于需要高可用性、数据完整性以及近乎不间断的主服务器维护等场景。

解决方案实践详情页面地址：https://www.huaweicloud.com/solution/implementations/deploy-a-highly-available-mha-mysql-cluster.html


**架构图**
---------------

![方案架构](./document/deploy-a-highly-available-MHA-Mysql-cluster.jpg)

**架构描述**
---------------
该解决方案会部署如下资源：

1. 创建3台Linux弹性云服务器，加入同一个云服务器组，配置反亲和策略，跨可用区部署，并分别安装MHA和MySQL软件。
2. 创建虚拟IP（VIP），用于MySQL数据库主从切换。
3. 创建3条弹性公网IP，用于MHA和MySQL环境部署及后期发生故障后发送告警邮件。
4. 创建安全组，可以保护弹性云服务器的网络安全，通过配置安全组规则，限定云服务器的访问端口。


**组织结构**
---------------

``` lua
huaweicloud-solution-deploy-a-highly-available-MHA-MySQL-cluster
├── deploy-a-highly-available-MHA-MySQL-cluster.tf.json -- 资源编排模板
├── userdata
    ├── initialize-master.sh  -- 主数据脚本配置文件
    ├── initialize-slave1.sh  -- 从数据脚本配置文件
    ├── initialize-slave2.sh  -- 从数据库及MHA Manager脚本配置文件
```

**开始使用**
---------------
该解决方案默认创建三台弹性云服务器，分别安装Mysql-5.7.34版本的数据库，一主（后缀名为matser）两从（后缀名为slave1、slave2）。MySQL管理用户用户组默认创建mysql，服务端口默认开启3306。主数据库上创建主从复制账户repl，密码同ECS初始化密码，允许登录地址为192.168.100.0/24网段。主数据库绑定虚拟IP，主从切换时，虚拟IP自动切换至新的主数据库。

MHA Manager安装在从数据库slave2中，一个MHA可以管理多套主从，只需要创建不同的配置文件即可，方案初始化时默认创建一套主从配置文件，MHA管理的用户默认mha，密码为ECS初始化密码，配置文件路径：/mha/conf/app1.cnf。


1、登陆[华为云服务器控制台](https://console.huaweicloud.com/ecm/?agencyId=084d9251a8bf46ef9c4d7c408f8b21e8&locale=zh-cn&region=cn-north-4#/ecs/manager/vmList)，找到该方案创建的三台弹性云服务器，使用远程连接工具登陆服务器。

图1 弹性云服务器
![弹性云服务器](./document/readme-image-001.png)


2、进入两台从数据库，输入以下命令，查看主从复制状态：
``` lua
mysql -uroot
SHOW SLAVE STATUS\G;
```
图2 主从复制状态

![主从复制状态](./document/readme-image-002.png)

3、登陆MHA Manager管理服务器，输入：masterha_check_ssh  --conf=/mha/conf/app1.cnf，检查主从数据库互信状态。

图3 主从数据库互信状态
![主从数据库互信状态](./document/readme-image-003.png)


4、在MHA Manager管理服务器，输入：masterha_check_repl  --conf=/mha/conf/app1.cnf，检查主从数据库状态。

图4 主从数据库状态

![主从数据库状态](./document/readme-image-004.png)

5、在MHA Manager管理服务器，输入：mha_app1_status，检查MHA运行状态。

图5 MHA运行状态

![MHA运行状态](./document/readme-image-005.png)

6、在MHA Manager管理服务器，输入：tail -f /mha/logs/manager，查看日志变化。

7、当发生故障时，MHA Manager管理服务器会自动完成主从切换，并发送告警邮件。用户收到报警邮件后可以去人工检查主数据的故障并处理。完成一次故障转移后，MHA Manager会自动停止，需要用户手动去启动。

图6 故障切换
![故障切换](./document/readme-image-006.png)

8、在新的主数据库（192.168.100.112）查看VIP漂移成功：
```lua
ifconfig
```
图7 VIP挂载

![VIP挂载](./document/readme-image-007.png)

9、在主数据库上重新启动MySQL服务，将其作为从数据库加入集群。

``` lua
systemctl start mysqld.service
mysql -uroot
CHANGE MASTER TO
      MASTER_HOST='192.168.100.112',
      MASTER_USER='repl',
      MASTER_PASSWORD='密码',
      MASTER_PORT=3306,
      MASTER_CONNECT_RETRY=10,
      MASTER_AUTO_POSITION=1;
START SLAVE;
SHOW SLAVE STATUS\G;
```
10、修改MHA Manager配置文件，将旧主数据库加入集群：
``` lua
vim /mha/conf/app1.cnf
[server1]
candidate_master=1
check_repl_delay=0
hostname=192.168.100.111
port=3306
systemctl start mysqld.service
```
11、在MHA Manager服务器上重新开启MHA服务即可。
``` lua
mha_app1_start
mha_app1_status
```
12、手动切换主数据库，必须先停止MHA服务：
```lua
mha_app1_stop
```

13、然后在MHA Manager上运行以下命令，进行手动在线切换主从数据库：
```lua
masterha_master_switch --conf=/mha/conf/app1.cnf --master_state=alive --new_master_host=192.168.0.111 --orig_master_is_new_slave --running_updates_limit=10000 --interactive=0
出现Switching master to 192.168.0.111(192.168.0.111:3306) completed successfully. 代表切换成功
```
图8 手动在线切换
![手动在线切换](./document/readme-image-008.png)

14、在新的主数据库（192.168.100.112）查看VIP漂移成功：
```lua
ifconfig
```

14、在MHA Manager服务器上重新开启MHA服务即可：
``` lua
mha_app1_start
mha_app1_status
```